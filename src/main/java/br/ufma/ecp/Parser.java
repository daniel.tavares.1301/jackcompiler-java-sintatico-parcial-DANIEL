package br.ufma.ecp;

import static br.ufma.ecp.token.TokenType.*;


import br.ufma.ecp.token.Token;
import br.ufma.ecp.token.TokenType;

public class Parser {


    private static class ParseError extends RuntimeException {}

    private Scanner scan; // Parser pede o token pro scanner
    private Token currentToken; //o que percorrerá o array
    private Token peekToken; // usa-se este token para que ele receba o próximo token, em nextToken
    private StringBuilder xmlOutput = new StringBuilder();

    public Parser(byte[] input) {
        scan = new Scanner(input); //Parser interage com o Scanner e ele que recebe a entrada do arquivo
        nextToken(); // pego o próximo token
    }

    private void nextToken() {
        currentToken = peekToken; // recebe o token atual para que na memória tenham 2 tokens juntos para verificar. Ex: preco[10]
        peekToken = scan.nextToken(); // pego o próximo token
    }

    void parse() {
        parseClass();
    }

    // classe
    // classdef -> 'class' className '{' classVarDec* subroutineDec* '}'
    void parseClass() {
        printNonTerminal("class");
        expectPeek(CLASS);
        expectPeek(IDENTIFIER);
        expectPeek(LBRACE);
        while (peekToken.type == FIELD || peekToken.type == STATIC) { // field int x, y;  || static int pointCount;
            parseClassVarDec(); // variáveis de classe ou de instância
        }

        while (peekToken.type == CONSTRUCTOR || peekToken.type == FUNCTION || peekToken.type == METHOD){
            parseSubroutineDec(); 
        }
        expectPeek(RBRACE);

        printNonTerminal("/class");
    }

    // varíaveis de classe
    // ( 'static' | 'field' ) type varName ( ',' varName)* ';'
    // Compiles a static declaration or a field declaration.
    // field int x, y; 
    // static int pointCount;
    void parseClassVarDec() {
        printNonTerminal("classVarDec");

        expectPeek(FIELD, STATIC);
        // 'Int' || 'Char' || 'Boolean' || className
        expectPeek(INT, CHAR, BOOLEAN, IDENTIFIER);
        expectPeek(IDENTIFIER);
        while (peekToken.type == COMMA) {
            expectPeek(COMMA);
            expectPeek(IDENTIFIER);
        }
        expectPeek(SEMICOLON);

        printNonTerminal("/classVarDec");
    }

    // subrotina da classe
    // ( 'constructor' | 'function' | 'method' ) ( 'void' | type) subroutineName '(' parameterList ')' subroutineBody
    void parseSubroutineDec() {
        printNonTerminal("subroutineDec");

        expectPeek(CONSTRUCTOR, FUNCTION, METHOD);
        // tipo do retorno
        // void | 'int' | 'char' | 'boolean' | className
        expectPeek(VOID, INT, CHAR, BOOLEAN, IDENTIFIER);
        //subroutineName
        expectPeek(IDENTIFIER);

        expectPeek(LPAREN);
        parseParameterList();
        expectPeek(RPAREN);
        parseSubroutineBody();

        printNonTerminal("/subroutineDec");
    }

    // Compila uma lista de parâmetros (possivelmente vazia), não incluindo o delimitador "()" .
    // ((type varName) ( ',' type varName)*)?
    void parseParameterList() {
        printNonTerminal("parameterList");

        if (!peekTokenIs(RPAREN)) { // se tiver pelo menos uma expressão
            expectPeek(INT,CHAR,BOOLEAN,IDENTIFIER);
            expectPeek(IDENTIFIER);
        }
        while (peekTokenIs(COMMA)){ // enquanto o próximo token for ','
            expectPeek(COMMA);
            expectPeek(INT,CHAR,BOOLEAN,IDENTIFIER);
            expectPeek(IDENTIFIER);
        }
   
        printNonTerminal("/parameterList");
    }

    // chamada da subrotina
    // implementação a partir do '.'
    // subroutineName '(' expressionList ')' | (className|varName) '.' subroutineName '(' expressionList ')'
    // ex: do Output.printString(”The average is ”);
    void parseSubroutineCall() {
        var nArgs = 0;

        if (peekTokenIs(LPAREN)) { // método da propria classe
            expectPeek(LPAREN);
            nArgs = parseExpressionList() + 1;
            expectPeek(RPAREN);

        } else {
            // pode ser um metodo de um outro objeto ou uma função
            expectPeek(DOT);
            expectPeek(IDENTIFIER); // nome da função

            expectPeek(LPAREN);
            nArgs += parseExpressionList();

            expectPeek(RPAREN);
        }

    }

    // antes da chamada da subrotina
    // para chamar métodos ou função que foram declarados com void ao invés de um tipo.
    // 'do' subroutineCall ';'
    // ex: do Output.printString(”The average is ”);
    void parseDo() {
        printNonTerminal("doStatement");

        expectPeek(DO);
        expectPeek(IDENTIFIER);
        parseSubroutineCall();
        expectPeek(SEMICOLON);

        printNonTerminal("/doStatement");
    }

    // variável do tipo var
    // 'var' type varName ( ',' varName)* ';'
    // não é possível atribuir valores no momento da declaração.
    // ex: var int variable, variable2;
    void parseVarDec() {
        printNonTerminal("varDec");

        expectPeek(VAR);
        // 'int' | 'char' | 'boolean' | className -> espera um tipo
        expectPeek(INT,CHAR,BOOLEAN,IDENTIFIER);
        expectPeek(IDENTIFIER); // nome da variável

        while (peekTokenIs(COMMA)) { // enquanto próximo token for ',', crio mais variáveis
            expectPeek(COMMA);
            expectPeek(IDENTIFIER);
        }

        expectPeek(SEMICOLON);

        printNonTerminal("/varDec");
    }

    // '{' varDec* statements '}'
    void parseSubroutineBody() {
        printNonTerminal("subroutineBody");

        expectPeek(LBRACE);
        
        while(peekTokenIs(VAR)){
            parseVarDec();
        }
        parseStatements();

        expectPeek(RBRACE);

        printNonTerminal("/subroutineBody");
    }

    // letStatement -> 'let' identifier '=' number ';'
    void parseLet() {
        printNonTerminal("letStatement");
        expectPeek(LET);
        expectPeek(IDENTIFIER);

        if (peekTokenIs(LBRACKET)) {
            expectPeek(LBRACKET);
            parseExpression();
            expectPeek(RBRACKET);
        }

        expectPeek(EQ);
        parseExpression();
        expectPeek(SEMICOLON);
        printNonTerminal("/letStatement");
    }

    // 'while' '(' expression ')' '{' statements '}'
    void parseWhile() {
        printNonTerminal("whileStatement");

        expectPeek(WHILE);
        expectPeek(LPAREN);
        parseExpression();
        expectPeek(RPAREN);
        expectPeek(LBRACE);
        parseStatements();
        expectPeek(RBRACE);

        printNonTerminal("/whileStatement");
    }

    // 'if' '(' expression ')' '{' statements '}' ( 'else' '{' statements '}' )?
    void parseIf() {
        printNonTerminal("ifStatement");

        expectPeek(IF);
        expectPeek(LPAREN);
        parseExpression();
        expectPeek(RPAREN);
        expectPeek(LBRACE);
        parseStatements();
        expectPeek(RBRACE);

        if(peekTokenIs(ELSE)){ // se próximo token for ELSE
            expectPeek(LBRACE);
            parseStatements();
            expectPeek(RBRACE);
        }

        printNonTerminal("/ifStatement");
    }

    //statement*
    void parseStatements() {
        printNonTerminal("statements");

        while (peekToken.type == WHILE ||
                peekToken.type == IF ||
                peekToken.type == LET ||
                peekToken.type == DO ||
                peekToken.type == RETURN) {
            parseStatement();
        }

        printNonTerminal("/statements");
    }

    //letStatement | ifStatement | whileStatement | doStatement | returnStatement
    void parseStatement() {
        switch (peekToken.type) {
            case LET:
                parseLet();
                break;
            case WHILE:
                parseWhile();
                break;
            case IF:
                parseIf();
                break;
            case DO:
                parseDo();
                break;
            case RETURN:
                parseReturn();
                break;
            default:
                throw new Error("Syntax error - expected a statement");
        }
    }

    // ReturnStatement -> 'return' expression? ';'
    void parseReturn() {
        printNonTerminal("returnStatement");

        expectPeek(RETURN);
        if (!peekTokenIs(SEMICOLON)) { // se o próximo token for diferente de ';', existe uma expressão
            parseExpression();
        }
        expectPeek(SEMICOLON);

        printNonTerminal("/returnStatement");
    }

    // expressionList -> (expression ( ',' expression)* )?
    int parseExpressionList() {
        printNonTerminal("expressionList");

        var nArgs = 0;

        if (!peekTokenIs(RPAREN)){ // verifica se tem pelo menos uma expressao
            parseExpression();
            nArgs = 1;
        }

        // procurando as demais expressões
        while (peekTokenIs(COMMA)) {
            expectPeek(COMMA);
            parseExpression();
            nArgs++;
        }

        printNonTerminal("/expressionList");
        return nArgs;

    }

    //expression -> term(op term)*
    void parseExpression() {
        printNonTerminal("expression");
        parseTerm();
        while (isOperator(peekToken.type)) {
            expectPeek(peekToken.type);
            parseTerm();
        }
        printNonTerminal("/expression");
    }


    // term -> number | identifier
    // integerConstant | stringConstant | keywordConstant | varName | varName '[' expression ']' | subroutineCall | '(' expression ')' | unaryOp term
    void parseTerm() {
        printNonTerminal("term");
        switch (peekToken.type) {
            case INTEGER:
                expectPeek(INTEGER);
                break;
            case STRING:
                expectPeek(STRING);
                break;
            case FALSE:
            case NULL:
            case TRUE:
                expectPeek(FALSE, NULL, TRUE);
                break;
            case THIS:
                expectPeek(THIS);
                break;
            case IDENTIFIER:
                expectPeek(IDENTIFIER);
                if (peekTokenIs(LPAREN) || peekTokenIs(DOT)) {
                    parseSubroutineCall();
                } else { // variavel comum ou array
                    if (peekTokenIs(LBRACKET)) { // array
                        expectPeek(LBRACKET);
                        
                        parseExpression();

                        expectPeek(RBRACKET);
        
                    }
                }
                break;
            case LPAREN:
                expectPeek(LPAREN);
                parseExpression();
                expectPeek(RPAREN);
                break;
            case MINUS:
            case NOT:
                expectPeek(MINUS, NOT);
                parseTerm();

    
                break;
            default:
                throw error(peekToken, "term expected");
        }

        printNonTerminal("/term");
    }

    // funções auxiliares
    public String XMLOutput() {
        return xmlOutput.toString();
    }

    private void printNonTerminal(String nterminal) {
        xmlOutput.append(String.format("<%s>\r\n", nterminal));
    }


    boolean peekTokenIs(TokenType type) { // reconhece tipo do próximo token
        return peekToken.type == type;
    }

    boolean currentTokenIs(TokenType type) { // reconhece tipo do token atual
        return currentToken.type == type;
    }

    private void expectPeek(TokenType... types) { // token esperado
        for (TokenType type : types) {
            if (peekToken.type == type) { 
                expectPeek(type);
                return;
            }
        }

       throw error(peekToken, "Expected a statement");

    }

    private void expectPeek(TokenType type) {
        if (peekToken.type == type) { // se o tipo do token atual existir, posso passar pra frente
            nextToken();
            xmlOutput.append(String.format("%s\r\n", currentToken.toString()));
        } else {
            throw error(peekToken, "Expected "+type.name());
        }
    }


    // marcação de erros
    private static void report(int line, String where,
        String message) {
            System.err.println(
            "[line " + line + "] Error" + where + ": " + message);
    }


    private ParseError error(Token token, String message) {
        if (token.type == TokenType.EOF) {
            report(token.line, " at end", message);
        } else {
            report(token.line, " at '" + token.value() + "'", message);
        }
        return new ParseError();
    }

}