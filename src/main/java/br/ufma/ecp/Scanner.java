package br.ufma.ecp;

import java.nio.charset.StandardCharsets;

import br.ufma.ecp.token.IdentifierToken;
import br.ufma.ecp.token.IntegerToken;
import br.ufma.ecp.token.KeywordToken;
import br.ufma.ecp.token.StringToken;
import br.ufma.ecp.token.SymbolToken;
import br.ufma.ecp.token.Token;
import br.ufma.ecp.token.TokenType;

// sua função é fazer a leitura do programa fonte, caractere a caractere, agrupar os caracteres em lexemas e 
// produzir uma sequência de símbolos léxicos conhecidos como tokens.
public class Scanner {

    private byte[] input; //trabalhamos com um array de bytes, pois dá de percorrer caracter por caracter
    private int current; //o que percorrerá o array
    private int start; //o primeiro número do dígito. Ex: 164; start=1

    private int line = 1;

    public Scanner(byte[] input) {
        this.input = input;
        current = 0;
        start = 0;
    }

    // reconhece + - numeros (1, 12, 678)
    public Token nextToken() { //função que pega o próximo token

        skipWhitespace(); // ignora espaço em branco e outros caracteres 

        start = current;
        char ch = peek();

        // abrange casos específicos do caracter
        switch (ch) {

            case '"':
                return string();

            case '/':
                if (peekNext() == '/') {
                    skipLineComments();
                    return nextToken();
                } else if (peekNext() == '*') {
                    skipBlockComments();
                    return nextToken();
                } else {
                    advance();
                    return new Token(TokenType.SLASH, line);
                }

                
            case 0: //0 representa o fim da fita
                return new Token(TokenType.EOF, line);
            default:

                if (Character.isDigit(ch)) { //quando encontro um dígito, tenta montar o número e não apenas retorna este dígito
                    return number();
                }

                if (Character.isLetter(ch)) { //se for uma letra, retorno um idenificador
                    return identifier();
                }

                if (TokenType.isSymbol(ch)) { // caso sejam sinais - + ou outros
                    return symbol();
                }

                throw new Error(line+":Unexpected character: "+peek());
        }

    }

    private void skipBlockComments() {
        boolean endComment = false; //fim do comentário
        advance();

        while (!endComment) {
            advance();
            char ch = peek();

            if (ch == 0) { // eof
                System.exit(1);
            }

            if (ch == '*') {
                //avançando e ignorando o comentário
                for (ch = peek(); ch == '*'; advance(), ch = peek()) 
                    ;

                // fim do comentário
                if (ch == '/') {
                    endComment = true;
                    advance();
                } 
            }

        }

    }

    private void skipLineComments() { //quebra de linha comentada

        for (char ch = peek(); ch != '\n' && ch != 0; advance(), ch = peek())
            if (ch == '\n')
                line++;
    }

    private void skipWhitespace() { // avança e/ou ignora esses caracteres
        char ch = peek();
        while (ch == ' ' || ch == '\r' || ch == '\t' || ch == '\n') {

            if (ch == '\n') // quebra de linha
                line++;

            advance();
            ch = peek();
        }
    }

    private boolean isAlphaNumeric(char ch) { // retorna true se for letra ou número
        return Character.isLetter(ch) || Character.isDigit(ch);
    }

    private Token string() {
        advance();
        start = current;
        //avança sobre a string até que feche as aspas duplas
        while (peek() != '"' && peek() != 0) { 
            advance();
        }
        String s = new String(input, start, current - start, StandardCharsets.UTF_8);
        Token token = new StringToken(s, line);
        advance();
        return token;
    }

    private Token identifier() { //reconhece letras 
        while (isAlphaNumeric(peek())) { //se caracter for uma letra, até mesmo com número junto, avança na fita
            advance();
        }
        String id = new String(input, start, current - start, StandardCharsets.UTF_8);
        TokenType type = TokenType.keyword(id); // reconhece palavras reservadas
        if (type == null) { // se token não for uma keyword, retorna o identificador
            type = TokenType.IDENTIFIER;
            return new IdentifierToken(id, line);
        } else { // se for, retorna a keyword
            return new KeywordToken(type, line);
        }

    }

    private Token number() { //reconhece numeros
        while (Character.isDigit(peek())) { //se caracter for um número, avança
            advance();
        }
        String s = new String(input, start, current - start, StandardCharsets.UTF_8);
        Token token = new IntegerToken(s, line);
        return token;
    }

    // função usada no switch para caso o caracter for um símbolo
    private SymbolToken symbol() {
        var ch = peek();
        advance();
        return new SymbolToken(TokenType.fromValue(String.valueOf(ch)), line);
    }


    //avança na fita enquanto não chega ao fim do arquivo
    private void advance() {
        char ch = peek();
        if (ch != 0) {
            current++;
        }
    }

    // função que pega o caracter atual do array
    private char peek() {
        if (current < input.length) { //se a posição do array for menor que seu tamanho, retorna o elemento da posição em char
            return (char) input[current];
        } else {
            return 0;
        }
    }

    // peekNext retorna o caracter após o caracter atual
    private char peekNext() {
        int next = current + 1;
        if (next < input.length) {
            return (char) input[next];
        } else {
            return 0;
        }
    }

}